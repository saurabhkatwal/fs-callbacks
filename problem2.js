function problem2() {
    let fs = require('fs');

    fs.readFile('./lipsum.txt', 'utf-8', (err, data) => {
        if (err)
            console.log(err);
        else {
            console.log("read file lipsum.txt");
            // console.log(data);
            let upperCaseData = data.toUpperCase();
            // console.log(upperCaseData);
            fs.writeFile("./created/newFileUpperCase.txt", upperCaseData, (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("create file newFileUpperCase.txt");
                    // console.log("File written successfully\n");
                    fs.writeFile("./created/filenames.txt", 'newFileUpperCase.txt\t', (err) => {
                        if (err)
                            console.log(err);
                        else {
                            console.log("create file filenames.txt");
                            // console.log("file created successfully");
                            fs.readFile('./created/newFileUpperCase.txt', 'utf-8', (err, dataNewFile) => {
                                if (err)
                                    console.log(err);
                                else {
                                    let lowerCaseData = dataNewFile.toLowerCase();
                                    // console.log(lowerCaseData);
                                    let sentences = lowerCaseData.split(".");
                                    // console.log(sentences);
                                    let sentenceText = "";
                                    for (let i = 0; i < sentences.length; i++) {
                                        sentenceText += sentences[i];
                                        sentenceText += '\n';
                                    }
                                    // console.log(sentenceText);
                                    fs.writeFile("./created/splitSentencesFile.txt", sentenceText, (err) => {
                                        if (err)
                                            console.log(err);
                                        else {
                                            console.log("create file splitSentencesFile.txt");
                                            // console.log("file created successfully: splitSentenceFile.txt");
                                            fs.appendFile("./created/filenames.txt", "splitSentencesFile.txt\t", (err) => {
                                                if (err) {
                                                    console.log(err);
                                                } else {
                                                    console.log("appending the file names to filename.txt");
                                                    // console.log("text appended successfully");
                                                    fs.readFile('./created/filenames.txt', 'utf-8', (err, data) => {
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            console.log("read file filenames.txt");
                                                            // console.log("filenames.txt data");
                                                            // console.log(data);
                                                            let fileNames = data.split('\t');
                                                            let file1 = fileNames[0];
                                                            let file2 = fileNames[1];
                                                            // console.log("file1 "+file1);
                                                            // console.log("file2 "+file2);
                                                            // console.log(file1.length);
                                                            // console.log(file2.length);
                                                            // console.log("filenames");
                                                            // console.log(typeof file1);
                                                            // console.log(typeof file2);
                                                            fs.readFile(`./created/${file1}`, 'utf-8', (err, data1) => {
                                                                if (err)
                                                                    console.log(err)
                                                                else {
                                                            
                                                                    // console.log("data1");
                                                                    // console.log(data1);
                                                                    fs.writeFile('temp.txt', data1, (err) => {
                                                                        if (err)
                                                                            console.log(err);
                                                                        else {
                                                                            console.log("create file temp.txt");
                                                                            // console.log("file written successfully: data1");
                                                                            fs.readFile(`./created/${file2}`, 'utf-8', (err, data2) => {
                                                                                // console.log("data2");
                                                                                // console.log(data2);
                                                                                fs.appendFile('temp.txt', data2, (err) => {
                                                                                    if (err) {
                                                                                        console.log(err);
                                                                                    } else {
                                                                                        console.log("appending data to temp.txt");
                                                                                        // console.log("data2 appended successfully");
                                                                                        fs.readFile('temp.txt', 'utf-8', (err, dataTemp) => {
                                                                                            let tempData = dataTemp;
                                                                                            // console.log("temp data");
                                                                                            // console.log(tempData);
                                                                                            let splitTempData = tempData.split('');
                                                                                            splitTempData.sort();

                                                                                            splitTempData = splitTempData.join("");
                                                                                            // console.log(splitTempData);
                                                                                            fs.writeFile("./created/sortedFile.txt", splitTempData, (err) => {})
                                                                                            fs.appendFile("./created/filenames.txt", "sortedFile.txt\t", (err) => {
                                                                                                if (err)
                                                                                                    console.log(err);
                                                                                                else {
                                                                                                    fs.readFile("./created/filenames.txt",'utf-8',(err,data)=>{
                                                                                                        if(err)
                                                                                                        console.log(err);
                                                                                                        else{
                                                                                                        let folder="./created/"
                                                                                                        const filesToDelete=data.split('\t');
                                                                                                        for(let i=0;i<filesToDelete.length-1;i++){
                                                                                                            console.log(filesToDelete[i] + ' : File Deleted Successfully.');
                                                                                                            fs.unlinkSync(folder + filesToDelete[i]);
                                                                                                        }
                                                                                                        fs.writeFile("./created/filenames.txt",'',(err)=>{
                                                                                                            if(err){
                                                                                                                console.log(err);
                                                                                                            }
                                                                                                            else{
                                                                                                                console.log("emptied the content of filenames.txt");
                                                                                                            }
                                                                                                        })
                                                                                                    }
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        })



                                                                                    }
                                                                                })
                                                                            })



                                                                        }
                                                                    })
                                                                }



                                                            })

                                                        }

                                            

                                                    })



                                                }
                                            });



                                        }

                                    })





                                }
                            })
                        }



                    })
                }



            })
        }


    })




}
module.exports=problem2;