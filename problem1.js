const fs = require("fs");

function createRandomJSONs() {
    fs.mkdir("./directory1", function (err) {
        if (err) {
            console.log(err)
        } else {
            console.log("New directory successfully created.")
            // for (let i = 0; i <= Math.floor(Math.random() * 10); i++) {
                fs.writeFile(`./directory1/mynewfile.json`, '{"name":"saurabh"}', function (err) {
                    if (err) throw err;
                    console.log('file created!');
                });
            // }

            let folder = './directory1/';

            fs.readdir(folder, (err, files) => {

                if (err) throw err;

                for (const file of files) {

                    console.log(file + ' : File Deleted Successfully.');

                    fs.unlinkSync(folder + file);

                }
            });
        }
    })

}
exports.createRandomJSONs = createRandomJSONs;
// function deleteFiles(){

// }
// exports.deleteFiles=deleteFiles;